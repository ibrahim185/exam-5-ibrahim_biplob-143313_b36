<?php

class myCalculator
    {
       public $value1, $value2;

    public function __construct($value_1, $value_2)
    {
        $this->value1=$value_1;
        $this->value2=$value_2;
    }


    public function add()
   {
     return $this->value1 + $this->value2;
    }

    public function sub()
    {
        return $this->value1 - $this->value2;
    }
    public function multiply()
    {
        return $this->value1 * $this->value2;
    }
    public function div()
    {
        return $this->value1 / $this->value2;
    }
}

$mycalc= new myCalculator(12,6);
echo $mycalc->add();
echo "<br/>";
echo $mycalc->sub();
echo "<br/>";
echo $mycalc->multiply();
echo "<br/>";
echo $mycalc->div();
